import {Router, Route} from "@vaadin/router";
import './app';

const routes: Route[] = [
    {
        path: '/',
        component: 'lit-app',
        children: [
            {
                path: 'blog',
                component: 'lit-blog',
                action: async () => {
                    await import('./blog/blog')
                },
                children: [
                    {
                        path: '',
                        redirect: '/blog/posts'
                    },
                    {
                        path:'posts',
                        component:'lit-blog-posts',
                        action:async ()=>{
                            await import('./blog/posts/posts')
                        }

                    },
                    {
                        path: 'posts/:id',
                        component: 'lit-blog-post',
                        action: async () => {
                            await import('./blog/post/post');
                        },
                    },
                ]
            },
            {
                path: 'about',
                component: 'lit-about',
                action: async () => {
                    await import('./about/about');
                },
            }
        ]
    }
]
const outlet = document.getElementById('outlet');
export const router = new Router(outlet);
router.setRoutes(routes);
import {customElement, html, LitElement} from "lit-element";

@customElement('lit-blog')
export class Blog extends LitElement{
    render(){
        return html`
        <slot></slot>
        `;
    }
}
import {customElement, html, css, LitElement, property} from "lit-element";
import {Post} from "../../../models/post";

@customElement('post-card')
export class PostCard extends LitElement {
    static styles = css`
      .blog-card {
        margin: 20px;
        display: flex;
        flex-direction: column;
        margin-bottom: 15px;
        background: white;
        border-radius: 5px;
        overflow: hidden;
      }

      .blog-description {
        padding: 20px;
        background: white;
      }

      .blog-footer {
        text-align: right;
      }

      .blog-link {
        cursor:pointer;
        color: #008cba;
      }
      .blog-link:hover{
        text-decoration: underline;
      }

      h1 {
        margin: 0;
        font-size: 1.5rem;
      }

      h2 {
        font-size: 1rem;
        font-weight: 300;
        color: #5e5e5e;
        margin-top: 5px;
      }
    `;

    @property({type: Object}) post?: Post;

    _readMoreHandler(){
      this.dispatchEvent(new CustomEvent('readMore',{detail:this.post,composed:true}))
    }

    render() {
        return html`
            <div class="blog-card">
                <div class="blog-description">
                    <h1>${this.post?.title}</h1>
                    <h2>${this.post?.author}</h2>
                    <p>${this.post?.description}</p>
                    <p class="blog-footer">
                        <a class="blog-link" @click=${this._readMoreHandler}>Read More</a>
                    </p>
                </div>
               
            </div>
        `;
    }
}
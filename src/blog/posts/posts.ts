import {css, customElement, html, LitElement,property} from "lit-element";
import {POSTS} from '../../../data/posts';
import {Post} from "../../../models/post";
import {Router} from "@vaadin/router";

@customElement('lit-blog-posts')
export class BlogPosts extends LitElement {
    static styles = css`
      h1 {
        color: blue;
      }
    `;
    @property({ type: Array }) blogPosts?: Post[];

    async _loadPostCard() {
        await import('../post-card/post-card')
    }
    firstUpdated(){
        this.blogPosts = POSTS;
        this.addEventListener('readMore',(e)=>{
            const post = (e as CustomEvent).detail as Post;
            Router.go(`/blog/posts/${post.id}`)
        })
    }
    render() {
        this._loadPostCard();
        return html`
            ${this.blogPosts?.map(post => {
                return html`
                    <post-card .post=${post}></post-card>
                `
            })}
        `;
    }


}